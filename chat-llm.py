from langchain_community.chat_models import ChatOllama
from langchain_core.messages import HumanMessage
import sys, os

# Set the environment variables from shell environment
LLM_MODEL = os.getenv("LLM_MODEL")
LLM_API_URL = os.getenv("LLM_API_URL")
LLM_JWT_BEARER = os.getenv("LLM_JWT_BEARER")


llm = ChatOllama(model=LLM_MODEL, base_url=LLM_API_URL,
headers={"Authorization": "Bearer " + LLM_JWT_BEARER,
"Content-Type":"application/json",})
# set variable ask from the second parameter set argument
if len(sys.argv) > 1:
    texte_question = sys.argv[1]
messages = [HumanMessage(content=texte_question)]
chat_model_response = llm.invoke(messages)
print(chat_model_response.content)