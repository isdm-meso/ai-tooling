import http from 'k6/http';
import { sleep, check } from 'k6';

export let options = {
  vus: 5,
  duration: '30s',
  //iterations: 1,
};

export default function () {
  const params = {
      headers: {
          'Authorization': 'Bearer ',
          'Content-Type': 'application/json',
      },
  };
  let response = http.post('https://isdm-chat.crocc.meso.umontpellier.fr/openai/completions',
    JSON.stringify({
      model: 'solidrust/Codestral-22B-v0.1-hf-AWQ',
      prompt: 'Wrote a c code to find pi ?',
    }), params);

  check(response, {
    'status was 200': (r) => r.status == 200,
    'transaction time OK': (r) => r.timings.duration < 45000,
  });
//  check(response).to.have.validJsonBody();

  sleep(1);
}
