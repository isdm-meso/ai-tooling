# Project Title

AI tooling for ISDM

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [License](#license)

## Installation

Dependancy
```
python -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## Usage

Create a file with followed variable, your model
```
export LLM_MODEL=mistral-small:24b-instruct-2501-q8_0
#export LLM_MODEL=solidrust/Codestral-22B-v0.1-hf-AWQ # or the second model
export LLM_API_URL=https://isdm-chat.crocc.meso.umontpellier.fr/api/
export LLM_API_KEY=
```

Source it
``` 
source yourfile.rc
```

Run
```
python chat-openai.py "my awesome query"
```

## License
Apache License 2.0. See the [LICENSE](LICENSE) file for more information.

## Contact

Cyril
